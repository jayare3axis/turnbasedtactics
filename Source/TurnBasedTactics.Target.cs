// Temp

using UnrealBuildTool;
using System.Collections.Generic;

public class TurnBasedTacticsTarget : TargetRules
{
	public TurnBasedTacticsTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "TurnBasedTactics" } );
	}
}
