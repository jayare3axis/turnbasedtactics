// Temp

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GridCamera.generated.h"

/**
* Controls panning, zooming and rotating the camera as well as following the current pawn if EnableFollowCam is set to true
*/
UCLASS()
class TURNBASEDTACTICS_API AGridCamera : public APawn
{
	GENERATED_UCLASS_BODY()
	
};
