// Temp

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include <Runtime/Engine/Public/TimerManager.h>
#include "DelayedTimerDelegate.generated.h"

/**
 * Utils for delayed operations.
 */
UCLASS()
class TURNBASEDTACTICS_API UDelayedTimerDelegate : public UObject
{
	GENERATED_BODY()

public:
	UDelayedTimerDelegate();
	
	FORCEINLINE void Init(FTimerDelegate _TimerDelegate, AActor* _WorldContextObject)
	{
		check(_WorldContextObject);

		TimerDelegate = _TimerDelegate;
		WorldContextObject = _WorldContextObject;

		DelayedTickDelegate.BindUFunction(this, FName("ProcessDelayedTick"));
	}

	/** Delegate will be executed after amount of given ticks, value of ticks must be higher than 1. */
	UFUNCTION()
	void DelayForTicks(int32 _Ticks);

private:
	UFUNCTION()
	void ProcessDelayedTick();

	FTimerDelegate TimerDelegate;
	AActor* WorldContextObject;
	int32 Ticks;

	FTimerDelegate DelayedTickDelegate;
};
