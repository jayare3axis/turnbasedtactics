// Temp

#pragma once

#include "GameplayTypes.generated.h"

UENUM(BlueprintType)
enum class ETurnState : uint8
{
	TS_GameLoading 			UMETA(DisplayName = "Game Loading"),
	TS_PreGameSetup 		UMETA(DisplayName = "Pre-Game Setup"),
	TS_TurnBasedCombat		UMETA(DisplayName = "Turn-Based Combat"),
	TS_FreeRoam				UMETA(DisplayName = "Free Roam"),
	TS_GameOver				UMETA(DisplayName = "Game Over"),
	TS_Other				UMETA(DisplayName = "Other")
};
