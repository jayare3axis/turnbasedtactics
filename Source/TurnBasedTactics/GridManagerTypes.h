// Temp

#pragma once

#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "GridManagerTypes.generated.h"

UENUM(BlueprintType)
enum class EHeight : uint8
{
	H_False 		UMETA(DisplayName = "false"),
	H_OneLevel 		UMETA(DisplayName = "One Level"),
	H_Multilevel	UMETA(DisplayName = "Multilevel")
};

USTRUCT(BlueprintType)
struct FEdges
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	TMap<int32, int32> Edges;
};

USTRUCT(BlueprintType)
struct FPathfinding
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	int32 Cost;

	UPROPERTY(BlueprintReadOnly)
	int32 Parent;

	FPathfinding()
	{
		Cost = 0;
		Parent = 0;
	}

	FPathfinding(int32 _Cost, int32 _Parent)
	{
		Cost = _Cost;
		Parent = _Parent;
	}
};

USTRUCT(BlueprintType)
struct FPathfindingMap
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	TMap<int32, FPathfinding> Paths;
};

USTRUCT(BlueprintType)
struct FPathfindingResult
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	FPathfindingMap PathsMap;

	UPROPERTY(BlueprintReadOnly)
	TSet<int32> ReachableUnits;

	UPROPERTY(BlueprintReadOnly)
	FPathfindingMap OpenListTiles;

	UPROPERTY(BlueprintReadOnly)
	int32 CurrentSearchStep;

	UPROPERTY(BlueprintReadOnly)
	TSet<int32> SearchedTiles;

	UPROPERTY(BlueprintReadOnly)
	FPathfindingMap ContinuePathTiles;
};

USTRUCT(BlueprintType)
struct FGridIndex
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	int32 X;

	UPROPERTY(BlueprintReadOnly)
	int32 Y;

	UPROPERTY(BlueprintReadOnly)
	int32 Z;
};

USTRUCT(BlueprintType)
struct FTwoInts
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	int32 Int1;

	UPROPERTY(BlueprintReadOnly)
	int32 Int2;
};

USTRUCT(BlueprintType)
struct FGridOptions
{
	GENERATED_BODY()

	/** The size in tiles of the grid in the X dimension */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	int32 GridSizeX;

	/** The size in tiles of the grid in the Y dimension */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	int32 GridSizeY;

	/** Is the default tile that makes up the grid walkable? If not, individual walkable tiles must be placed. */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	bool bCollisionPlaneWalkable;

	/** For debugging. Displays the tile indexes of all tiles in the viewport. Pregenerate Gameplay Grids must be set to true for this to work. */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	bool bDisplayTileIndexes;

	/** For debugging. Displays the edges of all tiles in the viewport. Pregenerate Gameplay Grids must be set to true for this to work. */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	bool bDisplayTileEdges;

	/** Generates some of the grids used for gameplay in the construction scripts o they won't have to load on Event Begin Play. Speeds up startup, but slows down the construction script. Activate before packaging */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	bool bPregenerateGameplayGrids;

	/** Will the default tile, and thus the initial grid, be visible? */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	bool bShowDefaultTile;

	/** Shows the collision plane that handles collision for the trace when the player clicks a tile. Make it visible if your default tile is invisible to place actors easily on the grid surface */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	bool bShowCollisionPlane;

	/** If set to false tiles will not be connected to diagonal tiles by default */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	bool bDiagonalMovement;

	/** Turns the meshes of all tile actors with "Instantiate" set to true to instanced static meshes */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	bool bInstantiate;

	/** If true, an extra map is used for walkability in addition to GridEdges, which allows tiles to have global costs, which can be simpler to work with, particularly for games requiring lots of live walkability modification. Interacts with the pathfinding types starting with Simple */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	bool bUseSimpleCosts;

	FGridOptions()
	{
		bCollisionPlaneWalkable = true;
		GridSizeX = 3;
		GridSizeY = 3;
		bShowDefaultTile = true;
		bDiagonalMovement = true;
		bUseSimpleCosts = true;
	}
};

USTRUCT(BlueprintType)
struct FProcedural
{
	GENERATED_BODY()

	/** Compares height of all adjacent tiles and sets walkability to false if the difference is more that HeightImpassableCutoff and to difference/heightSlowIncrement if lower than HeightImpassableCutoff but lower that HeightSlowIncrement */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	bool bAutoEdgeCostsBasedOnHeight;
	
	/** Shows a bounding box displaying the maximum and minimum Z location the Grid Manager will check for walkable tiles when using heightmaps */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	bool bShowHeightmapBoundingBox;

	/** Automaticalle traces between all adjacent tiles using RangeTrace and removes the edge between tiles if the trace hits. */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	bool bTraceForWalls;

	/** The height below the Grid Manager from which traces are done to find walkability. Meshes below this height will not be checked */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	float MinGridHeight;

	/** The height above the Grid Manager from which traces are done to find walkability. Meshes above this height will not be checked */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	float MaxGridHeight;

	/** If heightmap is set to multilevel, designates the maximum number of levels that will be generated. Any levels above this will be ignored. I recommend not having this higher than the maximum number of levels you want for your map, as this causes unneccessary processing. */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	float MaxLevels;

	/** See AutoHeightBasedEdgeCosts */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	float HeightSlowIncrement;

	/** See AutoHeightBasedEdgeCosts */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	float HeightImpassableCutoff;

	/** The minimum distance that must separate two levels on a multi-level grid */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	float HeightBetweenLevels;

	/** The height at which the Trace for Walls function should check for walls between tiles */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	float TraceForWallsHeight;

	/** false = no heightmap; OneLevel = traces from the sky (at MaxGridHeight) to the center of each tile once to generate a single layer heightmap; Multilevel = Traces from sky to each tile center, then continues downward and adds a level for each level found that is further from the previous than HeightBetweenLevels until MaxLevels or MinGridHeight is reached */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	EHeight Heightmap;

	/** Requires pregenerating gameplay grids. Add elements to this array and position them using widgets to specify what areas of a map can be reached. Is not necessary, but will prevent the hover marker from showing over tiles the player cannot reach. Uses pathfinding to find all tiles reachable from the specified locations, and removes all else from the grid. */
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Meta = (MakeEditWidget = true))
	TArray<FVector> ReachableTileMarks;

	FProcedural()
	{
		bShowHeightmapBoundingBox = true;
		HeightSlowIncrement = 100.f;
		MaxGridHeight = 1000.f;
		MaxLevels = 5;
		HeightImpassableCutoff = 100.f;
		HeightBetweenLevels = 200.f;
		TraceForWallsHeight = 100.f;
	}
};

USTRUCT(BlueprintType)
struct FStaticMeshes
{
	GENERATED_BODY()

	/** Mesh used to populate the ground level of a grid */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	UStaticMesh* DefaultTileMesh;

	FStaticMeshes()
	{
		// TODO for some reason this throws error, so it's set up using the editor
		static ConstructorHelpers::FObjectFinder<UStaticMesh> FoundDefaultTileMesh(TEXT("/Game/Core/Grid/SM_Tile_Square"));
		if (FoundDefaultTileMesh.Succeeded())
		{
			DefaultTileMesh = FoundDefaultTileMesh.Object;
		}
	}

};

USTRUCT(BlueprintType)
struct FLocationAndSize2D
{
	GENERATED_BODY()

	FVector Location;
	float ScaleX;
	float ScaleY;

	FLocationAndSize2D()
	{
		Location = FVector::ZeroVector;
		ScaleX = 0.f;
		ScaleY = 0.f;
	}

	FLocationAndSize2D(FVector& InLocation, float InScaleX, float InScaleY)
	{
		Location = InLocation;
		ScaleX = InScaleX;
		ScaleY = InScaleY;
	}
};
