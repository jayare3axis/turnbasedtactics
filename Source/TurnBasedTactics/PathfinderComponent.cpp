// Temp

#include "PathfinderComponent.h"
#include "GridManager.h"
#include "TurnManager.h"
#include "Unit.h"
#include "GridBlueprintFunctionLibrary.h"
#include "TBT_GameState.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

UPathfinderComponent::UPathfinderComponent()
{
}

void UPathfinderComponent::Setup(AGridManager* _Grid)
{
	check(_Grid);

	Grid = _Grid;
	GridSizeSquared = Grid->GetGridOptions().GridSizeX * Grid->GetGridOptions().GridSizeY;
}

void UPathfinderComponent::ActivatePathfinder()
{
	TurnManager = Cast<ATBT_GameState>( UGameplayStatics::GetGameState(GetWorld()) )->GetTurnManager();
}

void UPathfinderComponent::Run(int32 StartIndex, int32 MoveRange, EPathfindingType Type, FPathfindingResult& Result)
{
	FPathfindingMap LocalOpenListTiles;
	FPathfindingMap LocalPathsMap;
	FPathfindingMap LocalDelayedSearchTiles;
	FPathfindingMap LocalContinuePathTiles;
	TSet<int32> LocalReachableUnits;
	int32 LocalCurrentSearchStep;

	// Setup
	LocalOpenListTiles.Paths.Add(StartIndex, FPathfinding(0, StartIndex));
	LocalPathsMap.Paths.Add(StartIndex, FPathfinding(0, StartIndex));
	SearchedTiles.Empty();
	SearchedTiles.Add(StartIndex);

	// Pathfinding loop
	LocalCurrentSearchStep = 0;
	for (int32 i = LocalCurrentSearchStep; i < MoveRange; i++)
	{
		LocalCurrentSearchStep = i;
		LocalOpenListTiles = ChooseAndRunPathfindingSearchStep(Type, LocalCurrentSearchStep, LocalPathsMap, LocalReachableUnits, MoveRange, LocalOpenListTiles, LocalDelayedSearchTiles, LocalContinuePathTiles);

		// Add any nodes stored in the delayed search list
		LocalOpenListTiles.Paths.Append(LocalDelayedSearchTiles.Paths);
		LocalDelayedSearchTiles.Paths.Empty();

		// Stop pathfinding if there are no more tiles to search
		if (LocalOpenListTiles.Paths.Num() == 0)
		{
			break;
		}
	}

	// End Pathfinding
	if (TurnManager && TurnManager->GetActiveUnit() && TurnManager->GetActiveUnit()->GetMovementOptions().PathfindingType == EPathfindingType::PT_PassThroughFriendly)
	{
		LocalReachableUnits = KeepTargetsInSightFromMoveArray(LocalReachableUnits, 1, 0, TurnManager->GetActiveUnit()->GetMovementOptions().bDiamondShapedVisibility, false, LocalPathsMap);
	}

	Result.PathsMap = LocalPathsMap;
	Result.ReachableUnits = LocalReachableUnits;
	Result.OpenListTiles = LocalOpenListTiles;
	Result.CurrentSearchStep = LocalCurrentSearchStep;
	Result.SearchedTiles = SearchedTiles;
	Result.ContinuePathTiles = LocalContinuePathTiles;
}

FPathfindingMap UPathfinderComponent::ChooseAndRunPathfindingSearchStep(EPathfindingType Type, int32 SearchStep, FPathfindingMap& PathsMap, TSet<int32>& ReachableUnits, int32 Move, FPathfindingMap& OpenListTiles, FPathfindingMap& DelayedSearchTiles, FPathfindingMap& ContinuePathTiles)
{
	switch (Type)
	{
		case EPathfindingType::PT_Standard:
			return SearchAndAddAdjacentTiles(SearchStep, PathsMap, ReachableUnits, Move, OpenListTiles, DelayedSearchTiles, ContinuePathTiles);
		case EPathfindingType::PT_PassThroughFriendly:
			return SearchAndAddAdjacentTilesPassThroughFriendly(SearchStep, PathsMap, ReachableUnits, Move, OpenListTiles, DelayedSearchTiles, ContinuePathTiles);
		case EPathfindingType::PT_IgnoreCost:
			return SearchAndAddAdjacentTilesIgnoreCost(SearchStep, PathsMap, ReachableUnits, Move, OpenListTiles, DelayedSearchTiles, ContinuePathTiles);
		case EPathfindingType::PT_Big:
			return SearchAndAddAdjacentTilesBig(SearchStep, PathsMap, ReachableUnits, Move, OpenListTiles, DelayedSearchTiles, ContinuePathTiles);
		case EPathfindingType::PT_NoDiagonalMoovement:
			return SearchAndAddAdjacentTilesNoDiagonals(SearchStep, PathsMap, ReachableUnits, Move, OpenListTiles, DelayedSearchTiles, ContinuePathTiles);
		case EPathfindingType::PT_SimpleMixed:
			return SearchAndAddAdjacentTilesSimpleMixed(SearchStep, PathsMap, ReachableUnits, Move, OpenListTiles, DelayedSearchTiles, ContinuePathTiles);
		case EPathfindingType::PT_SimplePure:
			return SearchAndAddAdjacentTilesSimplePure(SearchStep, PathsMap, ReachableUnits, Move, OpenListTiles, DelayedSearchTiles, ContinuePathTiles);
		case EPathfindingType::PT_SimpleIgnoreCost:
			return SearchAndAddAdjacentTilesSimpleIgnoreCost(SearchStep, PathsMap, ReachableUnits, Move, OpenListTiles, DelayedSearchTiles, ContinuePathTiles);
	}

	FPathfindingMap Result;
	return Result;
}

FPathfindingMap UPathfinderComponent::SearchAndAddAdjacentTiles(int32 SearchStep, FPathfindingMap& PathsMap, TSet<int32>& ReachableUnits, int32 Move, FPathfindingMap& OpenListTiles, FPathfindingMap& DelayedSearchTiles, FPathfindingMap& ContinuePathTiles)
{
	const TArray<FEdges>& Edges = Grid->GetGridEdgesComponent()->GetEdges();

	FPathfindingMap Result;

	for (auto& Pair : OpenListTiles.Paths)
	{
		// If the parent tile cost is higher than the current search range, do not check now, but re-check next iteration
		if (SearchStep == Pair.Value.Cost)
		{
			for (auto& P : Edges[Pair.Key].Edges)
			{
				// Has the node already been searched (if so the cost should be > 0)
				if (!SearchedTiles.Contains(P.Key))
				{
					SearchedTiles.Add(P.Key);
					int32 Cost = Edges[Pair.Key].Edges[P.Key] + Pair.Value.Cost;
					if (Cost <= Move)
					{
						PathsMap.Paths.Add(P.Key, FPathfinding(Cost, Pair.Key));

						// Prevents units from walking through the spaces of other units
						if (Grid->GetGridUnits().Contains(P.Key))
						{
							ReachableUnits.Add(P.Key);
						}
						else
						{
							Result.Paths.Add(P.Key, FPathfinding(Cost, Pair.Key));
						}
					}
					else
					{
						ContinuePathTiles.Paths.Add(P.Key, FPathfinding(Cost, Pair.Key));
					}
				}
			}
		}
		else
		{
			DelayedSearchTiles.Paths.Add(Pair.Key, FPathfinding(Pair.Value.Cost, Pair.Value.Parent));
		}
	}

	return Result;
}

FPathfindingMap UPathfinderComponent::SearchAndAddAdjacentTilesPassThroughFriendly(int32 SearchStep, FPathfindingMap& PathsMap, TSet<int32>& ReachableUnits, int32 Move, FPathfindingMap& OpenListTiles, FPathfindingMap& DelayedSearchTiles, FPathfindingMap& ContinuePathTiles)
{
	const TArray<FEdges>& Edges = Grid->GetGridEdgesComponent()->GetEdges();

	FPathfindingMap Result;
	FPathfindingMap LocalOpenListOccupiedChildTiles;

	for (auto& Pair : OpenListTiles.Paths)
	{
		// If the parent tile cost is higher than the current search range, do not check now, but re-check next iteration
		if (SearchStep == Pair.Value.Cost)
		{
			for (auto& P : Edges[Pair.Key].Edges)
			{
				// Has the node already been searched (if so the cost should be > 0)
				if (!SearchedTiles.Contains(P.Key))
				{
					SearchedTiles.Add(P.Key);
					int32 Cost = Edges[Pair.Key].Edges[P.Key] + Pair.Value.Cost;
					if (Cost <= Move)
					{
						PathsMap.Paths.Add(P.Key, FPathfinding(Cost, Pair.Key));

						// Prevents units from walking through the spaces of other units
						if (Grid->GetGridUnits().Contains(P.Key))
						{
							ReachableUnits.Add(P.Key);

							if (TurnManager && TurnManager->GetActiveUnit() && TurnManager->GetActiveUnit()->GetAttributes().Faction != Grid->GetGridUnits()[P.Key]->GetAttributes().Faction)
							{
								LocalOpenListOccupiedChildTiles.Paths.Add(P.Key, FPathfinding(Cost, Pair.Key));
							}
							else
							{
								Result.Paths.Add(P.Key, FPathfinding(Cost, Pair.Key));
							}
						}
						else
						{
							Result.Paths.Add(P.Key, FPathfinding(Cost, Pair.Key));
						}
					}
					else
					{
						ContinuePathTiles.Paths.Add(P.Key, FPathfinding(Cost, Pair.Key));
					}
				}
			}
		}
		else
		{
			DelayedSearchTiles.Paths.Add(Pair.Key, FPathfinding(Pair.Value.Cost, Pair.Value.Parent));
		}
	}

	// TODO test - this is weird
	Result.Paths.Append(LocalOpenListOccupiedChildTiles.Paths);
	return Result;
}

FPathfindingMap UPathfinderComponent::SearchAndAddAdjacentTilesIgnoreCost(int32 SearchStep, FPathfindingMap& PathsMap, TSet<int32>& ReachableUnits, int32 Move, FPathfindingMap& OpenListTiles, FPathfindingMap& DelayedSearchTiles, FPathfindingMap& ContinuePathTiles)
{
	const TArray<FEdges>& Edges = Grid->GetGridEdgesComponent()->GetEdges();

	FPathfindingMap Result;

	for (auto& Pair : OpenListTiles.Paths)
	{
		for (auto& P : Edges[Pair.Key].Edges)
		{
			// Has the node already been searched (if so the cost should be > 0)
			if (!SearchedTiles.Contains(P.Key))
			{
				SearchedTiles.Add(P.Key);

				int32 Cost = Edges[Pair.Key].Edges[P.Key] + 1;
				PathsMap.Paths.Add(P.Key, FPathfinding(Cost, Pair.Key));

				// Prevents units from walking through the spaces of other units
				if (Grid->GetGridUnits().Contains(P.Key))
				{
					ReachableUnits.Add(P.Key);
				}
				else
				{
					Result.Paths.Add(P.Key, FPathfinding(Cost, Pair.Key));
				}
			}
		}
	}

	return Result;
}

FPathfindingMap UPathfinderComponent::SearchAndAddAdjacentTilesBig(int32 SearchStep, FPathfindingMap& PathsMap, TSet<int32>& ReachableUnits, int32 Move, FPathfindingMap& OpenListTiles, FPathfindingMap& DelayedSearchTiles, FPathfindingMap& ContinuePathTiles)
{
	FPathfindingMap Result;

	// TODO implement

	return Result;
}

FPathfindingMap UPathfinderComponent::SearchAndAddAdjacentTilesNoDiagonals(int32 SearchStep, FPathfindingMap& PathsMap, TSet<int32>& ReachableUnits, int32 Move, FPathfindingMap& OpenListTiles, FPathfindingMap& DelayedSearchTiles, FPathfindingMap& ContinuePathTiles)
{
	const TArray<FEdges>& Edges = Grid->GetGridEdgesComponent()->GetEdges();

	FPathfindingMap Result;

	for (auto& Pair : OpenListTiles.Paths)
	{
		// If the parent tile cost is higher than the current search range, do not check now, but re-check next iteration
		if (SearchStep == Pair.Value.Cost)
		{
			for (auto& P : Edges[Pair.Key].Edges)
			{
				// Has the node already been searched (if so the cost should be > 0)
				if (!SearchedTiles.Contains(P.Key))
				{
					if (UGridBlueprintFunctionLibrary::CheckIfStraightAdjacent(Grid, P.Key, Pair.Key))
					{
						SearchedTiles.Add(P.Key);
						int32 Cost = Edges[Pair.Key].Edges[P.Key] + Pair.Value.Cost;
						if (Cost <= Move)
						{
							PathsMap.Paths.Add(P.Key, FPathfinding(Cost, Pair.Key));

							// Prevents units from walking through the spaces of other units
							if (Grid->GetGridUnits().Contains(P.Key))
							{
								ReachableUnits.Add(P.Key);
							}
							else
							{
								Result.Paths.Add(P.Key, FPathfinding(Cost, Pair.Key));
							}
						}
						else
						{
							ContinuePathTiles.Paths.Add(P.Key, FPathfinding(Cost, Pair.Key));
						}
					}
				}
			}
		}
		else
		{
			DelayedSearchTiles.Paths.Add(Pair.Key, FPathfinding(Pair.Value.Cost, Pair.Value.Parent));
		}
	}

	return Result;
}

FPathfindingMap UPathfinderComponent::SearchAndAddAdjacentTilesSimpleMixed(int32 SearchStep, FPathfindingMap& PathsMap, TSet<int32>& ReachableUnits, int32 Move, FPathfindingMap& OpenListTiles, FPathfindingMap& DelayedSearchTiles, FPathfindingMap& ContinuePathTiles)
{
	FPathfindingMap Result;

	// TODO propably dont need this

	return Result;
}

FPathfindingMap UPathfinderComponent::SearchAndAddAdjacentTilesSimplePure(int32 SearchStep, FPathfindingMap& PathsMap, TSet<int32>& ReachableUnits, int32 Move, FPathfindingMap& OpenListTiles, FPathfindingMap& DelayedSearchTiles, FPathfindingMap& ContinuePathTiles)
{
	FPathfindingMap Result;

	// TODO propably dont need this

	return Result;
}

FPathfindingMap UPathfinderComponent::SearchAndAddAdjacentTilesSimpleIgnoreCost(int32 SearchStep, FPathfindingMap& PathsMap, TSet<int32>& ReachableUnits, int32 Move, FPathfindingMap& OpenListTiles, FPathfindingMap& DelayedSearchTiles, FPathfindingMap& ContinuePathTiles)
{
	FPathfindingMap Result;

	// TODO propably dont need this

	return Result;
}

TSet<int32> UPathfinderComponent::KeepTargetsInSightFromMoveArray(TSet<int32> TargetIndexes, int32 Range, int32 MinRange, bool DiamondShapedVisibility, bool bAvoidTileOccupiedByCurrentUnit, FPathfindingMap PathsMap)
{
	TSet<int32> Result;

	// TODO implement

	return Result;
}
