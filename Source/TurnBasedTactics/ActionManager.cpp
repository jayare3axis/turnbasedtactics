// Temp

#include "ActionManager.h"

AActionManager::AActionManager(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bReplicates = true;
	bReplicateMovement = false;
	NetPriority = 1.f;
	bCanBeDamaged = false;
	SpawnCollisionHandlingMethod = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	SetActorTickEnabled(false);
	SetActorHiddenInGame(true);
}
