// Temp

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ActionManager.generated.h"

/**
* Handles most actions that will be displayed to the players. Events are first calculated server side at grid-level, which then sends a list of actions to each client. These actions are then animated in order through this manager.
*/
UCLASS()
class TURNBASEDTACTICS_API AActionManager : public ACharacter
{
	GENERATED_UCLASS_BODY()

};
