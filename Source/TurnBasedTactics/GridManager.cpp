// Temp

#include "GridManager.h"
#include "Unit.h"
#include "GridBlueprintFunctionLibrary.h"
#include "TurnManager.h"
#include "ActionManager.h"
#include "TBT_GameState.h"
#include "CommonTypes.h"
#include "Net/UnrealNetwork.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

AGridManager::AGridManager(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PrimaryActorTick.bStartWithTickEnabled = false;
	bCanBeDamaged = false;
	bReplicates = true;
	
	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = Scene;

	InitCollisionPlane();
	InitHeightmapBox();

	DefaultTile = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("DefaultTile"));
	DefaultTile->SetupAttachment(Scene);

	GridLocations = CreateDefaultSubobject<UGridLocationsComponent>(TEXT("GridLocations"));
	GridEdges = CreateDefaultSubobject<UGridEdgesComponent>(TEXT("GridEdges"));
	Pathfinder = CreateDefaultSubobject<UPathfinderComponent>(TEXT("Pathfinder"));
}

void AGridManager::ActivateGridManager()
{
	GameStateRef = Cast<ATBT_GameState>( UGameplayStatics::GetGameState(GetWorld()) );

	check(GameStateRef);

	ActionManager = GameStateRef->GetActionManager();
	TurnManager = GameStateRef->GetTurnManager();

	GridLocations->ActivateGridLocations();
	GridEdges->ActivateGridEdges();
	Pathfinder->ActivatePathfinder();

	SetupGridCollision();

	if (!GridOptions.bPregenerateGameplayGrids)
	{
		GridLocations->Create(bGridHasHoles, GridSizeZ);
		GridEdges->Create();

		if (GridOptions.bUseSimpleCosts)
		{
			GridEdges->CreateSimpleCosts();
		}
	}

	bGridSetupDone = true;
}

void AGridManager::InitCollisionPlane()
{
	CollisionPlane = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CollisionPlane"));

	static ConstructorHelpers::FObjectFinder<UStaticMesh> CollisionPlaneMesh(TEXT("/Game/Core/Grid/SM_SimplePlane"));
	if (CollisionPlaneMesh.Succeeded())
	{
		CollisionPlane->SetStaticMesh(CollisionPlaneMesh.Object);
	}

	static ConstructorHelpers::FObjectFinder<UMaterial> CollisionPlaneMaterial(TEXT("/Game/Core/Grid/M_Blue_Transparent"));
	if (CollisionPlaneMaterial.Succeeded())
	{
		CollisionPlane->SetMaterial(0, CollisionPlaneMaterial.Object);
	}

	CollisionPlane->SetNotifyRigidBodyCollision(true);
	CollisionPlane->SetCollisionProfileName(TEXT("BlockAll"));
	CollisionPlane->bGenerateOverlapEvents = false;

	CollisionPlane->CastShadow = false;

	CollisionPlane->SetupAttachment(Scene);
}

void AGridManager::InitHeightmapBox()
{
	HeightmapBox = CreateDefaultSubobject<UBoxComponent>(TEXT("HeightmapBox"));

	HeightmapBox->SetCollisionProfileName(TEXT("OverlapAll"));
	HeightmapBox->bGenerateOverlapEvents = false;
	HeightmapBox->SetBoxExtent(FVector(100.f, 100.f, 100.f));

	HeightmapBox->SetupAttachment(Scene);
}

void AGridManager::OnConstruction(const FTransform& Transform)
{
	AActor::OnConstruction(Transform);

	PreventIncompatibleVariableVariations();
	SetupScaleAndDefaultTiles();
	SetupCollisionPlane();
	SetupHeightmapBox();	

	GridLocations->Setup(this);
	GridEdges->Setup(this);
	Pathfinder->Setup(this);

	SpawnTileGrid();

	if (GridOptions.bPregenerateGameplayGrids)
	{
		PregenerateGameplayGrids();
	}
}

void AGridManager::PreventIncompatibleVariableVariations()
{
	if (Procedural.HeightSlowIncrement > Procedural.HeightImpassableCutoff)
	{
		Procedural.HeightSlowIncrement = Procedural.HeightImpassableCutoff;
	}

	if (Procedural.Heightmap == EHeight::H_Multilevel)
	{
		Procedural.bAutoEdgeCostsBasedOnHeight = true;
	}
}

void AGridManager::SetupScaleAndDefaultTiles()
{
	GridSizeSquared = GridOptions.GridSizeX * GridOptions.GridSizeY;

	DefaultTile->ClearInstances();

	DefaultTile->SetStaticMesh(StaticMeshes.DefaultTileMesh);
	DefaultTile->SetVisibility(GridOptions.bShowDefaultTile);

	TileXSize = StaticMeshes.DefaultTileMesh->GetBounds().GetBox().GetSize().X;
	TileYSize = TileXSize;
}

void AGridManager::SetupCollisionPlane()
{
	FLocationAndSize2D LocAndSize = SetCollisionPlaneAndHeightmapBoxScaleAndLocation();

	CollisionPlane->SetRelativeTransform(
		FTransform(
			FRotator::ZeroRotator,
			LocAndSize.Location,
			FVector(LocAndSize.ScaleX, LocAndSize.ScaleY, 1.f)
		)
	);

	CollisionPlane->SetVisibility(GridOptions.bShowCollisionPlane);
}

void AGridManager::SetupHeightmapBox()
{
	if (Procedural.Heightmap == EHeight::H_False)
	{
		HeightmapBox->SetVisibility(false);
		return;
	}

	FLocationAndSize2D LocAndSize = SetCollisionPlaneAndHeightmapBoxScaleAndLocation();

	HeightmapBox->SetRelativeTransform(
		FTransform(
			FRotator::ZeroRotator,
			FVector(LocAndSize.Location.X, LocAndSize.Location.Y, (Procedural.MinGridHeight + Procedural.MaxGridHeight) / 2.f),
			FVector(LocAndSize.ScaleX, LocAndSize.ScaleY, (Procedural.MaxGridHeight - Procedural.MinGridHeight) / 200.f)
		)
	);

	HeightmapBox->SetVisibility(Procedural.bShowHeightmapBoundingBox);
}

void AGridManager::SpawnTileGrid()
{
	if (!GridOptions.bShowDefaultTile)
	{
		return;
	}

	for (int32 i = 0; i < GridSizeSquared; i++)
	{
		FVector Location = GridLocations->ConvertIndexToLocationSquareGrid(i);
		DefaultTile->AddInstance(FTransform(FRotator::ZeroRotator, Location, FVector::OneVector));
	}
}

void AGridManager::PregenerateGameplayGrids()
{
	GridLocations->Create(bGridHasHoles, GridSizeZ);
	GridEdges->Create();
	//MakeBigUnitArray();

	if (GridOptions.bDisplayTileIndexes)
	{
		DisplayTileIndexes();
	}

	if (GridOptions.bDisplayTileEdges)
	{
		DisplayTileEdges();
	}

	// Turns actors with "Instantiate" set to true to instances static meshes in the viewport, 
	// which can greatly increase performance. Only works for tiles with single meshes and a single, regular material.
	if (GridOptions.bInstantiate)
	{
		bInstantiating = true;
		ReplaceActorMeshesWithInstances();
	}
	else
	{
		ConvertInstancesToActors();
	}
}

/*void AGridManager::MakeBigUnitArray()
{
}*/

void AGridManager::ReplaceActorMeshesWithInstances()
{
	// TODO implement
}

void AGridManager::ConvertInstancesToActors()
{
	// TODO implement
}

FLocationAndSize2D AGridManager::SetCollisionPlaneAndHeightmapBoxScaleAndLocation()
{
	FVector Location = FVector(
		GridOptions.GridSizeX * TileXSize / 2 - TileXSize / 2, 
		GridOptions.GridSizeY * TileYSize / 2 - TileYSize / 2,
		0.1f
	);

	return FLocationAndSize2D(
		Location,
		GridOptions.GridSizeX * TileXSize / 200.f,
		GridOptions.GridSizeY * TileYSize / 200.f
	);
}

void AGridManager::SetupGridCollision()
{
	if (GridOptions.bShowDefaultTile)
	{
		DefaultTile->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	(GridOptions.bCollisionPlaneWalkable) ?
		CollisionPlane->SetCollisionResponseToChannel(COLLISION_PATH, ECollisionResponse::ECR_Block) :
		CollisionPlane->SetCollisionResponseToChannel(COLLISION_PATH, ECollisionResponse::ECR_Ignore);
}

void AGridManager::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	DOREPLIFETIME(AGridManager, bGridSetupDone);
}
