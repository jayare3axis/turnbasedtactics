// Temp

#include "TBT_GameMode.h"
#include "TBT_PlayerController.h"
#include "TBT_GameState.h"
#include "TBT_HUD.h"
#include "GridCamera.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

// TODO remove when ready
#include <EngineGlobals.h>
#include <Runtime/Engine/Classes/Engine/Engine.h>
#include <Runtime/Engine/Public/TimerManager.h>

ATBT_GameMode::ATBT_GameMode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	DefaultPawnClass = AGridCamera::StaticClass();
	PlayerControllerClass = ATBT_PlayerController::StaticClass();
	GameStateClass = ATBT_GameState::StaticClass();
	HUDClass = ATBT_HUD::StaticClass();
}

void ATBT_GameMode::BeginPlay()
{
	ActivateTBT();
}

void ATBT_GameMode::ActivateTBT()
{
	GameStateRef = Cast<ATBT_GameState>( UGameplayStatics::GetGameState(GetWorld()) );

	check(GameStateRef);

	GameStateRef->SetupCoreActors();

	for (APlayerController* PC : WaitingControllers)
	{
		ATBT_PlayerController* TBT_PC = Cast<ATBT_PlayerController>(PC);
		check(TBT_PC);
		TBT_PC->Setup();
	}

	WaitingControllers.Empty();

	// Wait for all actors to finish setup before we start the game.
	FTimerDelegate TimerDelegate;
	TimerDelegate.BindUFunction(this, FName("OnActivated"));

	ActivatedDelegate = NewObject<UDelayedTimerDelegate>();
	ActivatedDelegate->Init(TimerDelegate, this);
	ActivatedDelegate->DelayForTicks(2);
}

void ATBT_GameMode::OnActivated()
{
	// Switches from GameLoading to TurnBasedCombat. This signals the turn manager that the game may start.
	GameStateRef->SetTurnState(ETurnState::TS_TurnBasedCombat);
}
