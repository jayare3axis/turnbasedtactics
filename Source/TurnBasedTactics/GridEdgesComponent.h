// Temp

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GridManagerTypes.h"
#include "GridEdgesComponent.generated.h"

// Forward
class AGridManager;
class AGA_Tile;
class ATurnManager;

/**
* Handles creating, altering and storing edges for the grid.
*/
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TURNBASEDTACTICS_API UGridEdgesComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UGridEdgesComponent();

	/** Sets up parameters for edges generation. */
	void Setup(AGridManager* Grid);

	/** Activates the component. */
	void ActivateGridEdges();

	/** Generates all grid edges, determining what tiles can be moved between and the movement cost for each edge */
	void Create();

	/** If bUseSimpleCosts is true, adds the simple costs of all tiles to the GridSimpleCosts map. By default this map is checked by any units with a pathfinding type with "simple" in its name */
	void CreateSimpleCosts();

	/** Gets the edges. */
	FORCEINLINE const TArray<FEdges>& GetEdges() const
	{
		return Edges;
	}

	/** Adds a single edge to an index of the edge array */
	void AddTileEdge(int32 TileIndex, int32 Edge, int32 Cost);

	/** Removes the specified edge from the edges array. */
	void RemoveTileEdge(int32 TileIndex, int32 Edge);

	/** Removes the edges connecting two tiles in both directions, making it impossible to move directly between them. */
	void RemoveTileEdgeBothWays(int32 Index1, int32 Index2);

	/** Sets the cost of an existing edge to a different cost as specified. */
	void SetEdgeCost(int32 TileIndex, int32 Edge, int32 NewCost);

protected:
	/** Generates some of the grids used for gameplay in the construction scripts o they won't have to load on Event Begin Play. Speeds up startup, but slows down the construction script. Activate before packaging. */
	void CreateBaseEdges();

	/** Removes any edges pointing from the indexes at the edges to the grid to indexes outside these grid indexes (which are invalid at the north and south edge and wraps around to the other side of the grid for the east and west edges) */
	void RemoveEdgesOnGridBorders();

	/** Generates some of the grids used for gameplay in the construction scripts o they won't have to load on Event Begin Play. Speeds up startup, but slows down the construction script. Activate before packaging. */
	void SetEdgesBasedOnTerrain();

	/** Helper fuction used by 'SetEdgesBasedOnTerrain'. */
	bool CheckIfValidEdgeCandidate(int32 EdgeIndex, int32 TileIndex, bool bTraceForWalls, bool& AlreadyContains, int32& EdgeCost);

	/** Compares height difference of two tiles. Returns the appropriate edge cost between them based on the values specified. If the difference is larger than the Height Impassable Cutoff, returns 0 (indicating that the edge should be removed). If the difference is lower than Height Impassable Cutoff, but higher than Height Slow Increment, returns a value corresponding to the height difference divided by the height slow increment. */
	int32 GetEdgeCostFromZDifference(float ParentZ, int32 GridIndex);

	/**  Gets the edge costs of all tile actors and add them to the edge array. */
	void AddAllTileActorEdges();

	/** Adds the edges of a tile to the edge array by adding them to the index of the tile as well as connection up neighboring tiles to this tile appropriately */
	void AddTileEdgesToGridEdgesArray(AGA_Tile* Tile);

	/** If the ReachableTileMarkers array is not empty, runs pathfinding from each of these markers, making any tiles that are not found impassable. Used mostly for looks, so that the hover marker does not appear over tiles the player could never reach. */
	void RemoveUnreachableTiles();

	/** Holds the edges of all tiles, designating which tiles can be moved to from any given tile and what the cost of movement is */
	UPROPERTY(BlueprintReadOnly, Category = "GridArrays")
	TArray<FEdges> Edges;

	/** Stores GridEdges before heightmaps are added, for cases where one wants to ignore heightmap modifications of the grid. Use GridEdges in most cases. */
	UPROPERTY(BlueprintReadOnly, Category = "GridArrays")
	TArray<FEdges> BaseEdges;

	/** Stores simple costs. */
	UPROPERTY(BlueprintReadOnly, Category = "GridArrays")
	TMap<int32, int32> SimpleCosts;

	AGridManager* Grid;
	int32 GridSizeSquared;

#pragma region
	UPROPERTY()
	ATurnManager* TurnManager;
#pragma endregion References
};
