// Temp

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DelayedTimerDelegate.h"
#include "TBT_GameMode.generated.h"

// Forward
class ATBT_GameState;

/**
 * Sets up the game and handles login events.
 */
UCLASS()
class TURNBASEDTACTICS_API ATBT_GameMode : public AGameModeBase
{
	GENERATED_UCLASS_BODY()

public:
	/* AActor */
	virtual void BeginPlay() override;

protected:
	/** Called on begin play. */
	void ActivateTBT();

	/** Called when everything is set up. */
	UFUNCTION()
	void OnActivated();

#pragma region
	UPROPERTY()
	ATBT_GameState* GameStateRef;
#pragma endregion References	

private:
	TSet<APlayerController*> WaitingControllers;
	FTimerHandle ActivateTimerHandler;

	UPROPERTY()
	UDelayedTimerDelegate* ActivatedDelegate;
};
