// Temp

#include "GA_Tile.h"

AGA_Tile::AGA_Tile(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	SimpleCost = 1;

	// TODO this is just for test

	Edges.Add(-3);
	Edges.Add(1);
	Edges.Add(3);
	Edges.Add(-1);
	Edges.Add(-2);
	Edges.Add(4);
	Edges.Add(2);
	Edges.Add(-4);

	EdgeCosts.Edges.Add(-3, 3);
	EdgeCosts.Edges.Add(1, 3);
	EdgeCosts.Edges.Add(3, 3);
	EdgeCosts.Edges.Add(-1, 3);
	EdgeCosts.Edges.Add(-2, 3);
	EdgeCosts.Edges.Add(4, 3);
	EdgeCosts.Edges.Add(2, 3);
	EdgeCosts.Edges.Add(-4, 3);

	GridIndex = 0;
}
