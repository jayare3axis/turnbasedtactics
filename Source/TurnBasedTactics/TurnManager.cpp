// Temp

#include "TurnManager.h"
#include "Unit.h"
#include "Net/UnrealNetwork.h"

ATurnManager::ATurnManager(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bReplicates = true;
	bAlwaysRelevant = true;
	bNetLoadOnClient = false;
	NetUpdateFrequency = 10.f;
	bCanBeDamaged = false;
	bFindCameraComponentWhenViewTarget = false;

	SetActorTickEnabled(false);
}

void ATurnManager::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	DOREPLIFETIME(ATurnManager, ActiveUnit);
}

void ATurnManager::Initialize()
{
	// TODO implement
}
