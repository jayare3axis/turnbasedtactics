// Temp

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TurnManager.generated.h"

// Forward
class AUnit;

/**
* Handles starting and ending unit turns, holds the initiative order, current turn, current active unit and functions related to manipulating the turn order.
*/
UCLASS()
class TURNBASEDTACTICS_API ATurnManager : public AActor
{
	GENERATED_UCLASS_BODY()

	/** Initializes Turn Mabager. */
	void Initialize();

	/** Gets active unit. */
	FORCEINLINE AUnit* GetActiveUnit() const
	{
		return ActiveUnit;
	}

protected:
	/** Holds active unit. */
	UPROPERTY(BlueprintReadOnly, Replicated, Category = "Default")
	AUnit* ActiveUnit;
	
};
