// Temp

#include "TBT_GameState.h"
#include "GridManager.h"
#include "TurnManager.h"
#include "ActionManager.h"
#include "Net/UnrealNetwork.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

ATBT_GameState::ATBT_GameState(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void ATBT_GameState::SetTurnState(ETurnState NewTurnState)
{
	TurnState = NewTurnState;
}

void ATBT_GameState::SetupCoreActors()
{
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AGridManager::StaticClass(), FoundActors);
	check(FoundActors[0]);
	GridManager = Cast<AGridManager>(FoundActors[0]);

	check(TurnManagerClass);
	TurnManager = Cast<ATurnManager>( GetWorld()->SpawnActor(TurnManagerClass) );
	TurnManager->Initialize();

	check(ActionManagerClass);
	ActionManager = Cast<AActionManager>(GetWorld()->SpawnActor(ActionManagerClass) );

	// Activate the grid manager after making the initiative order array, as this is used in the setup of BP_GridManager.
	GridManager->ActivateGridManager();
}

void ATBT_GameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	DOREPLIFETIME(ATBT_GameState, TurnState);
	DOREPLIFETIME(ATBT_GameState, GridManager);
	DOREPLIFETIME(ATBT_GameState, TurnManager);
	DOREPLIFETIME(ATBT_GameState, ActionManager);
}
