// Temp

#include "GridBlueprintFunctionLibrary.h"

FVector UGridBlueprintFunctionLibrary::ConvertFromGridToWorld(AGridManager* Grid, FVector Location)
{
	FTransform Result = FTransform(Grid->GetActorTransform()); // Is copy needed here?
	return Result.TransformPosition(Location);
}

FVector UGridBlueprintFunctionLibrary::ConvertFromWorldToGrid(AGridManager* Grid, FVector Location)
{
	FTransform Result = FTransform(Grid->GetActorTransform()); // Is copy needed here?
	return Result.InverseTransformPosition(Location);
}

FGridIndex UGridBlueprintFunctionLibrary::ConvertGridIndexToStruct(AGridManager* Grid, int32 Index)
{
	FGridIndex Result;
	const FGridOptions& GridOptions = Grid->GetGridOptions();

	int32 GridSizeSquared = GridOptions.GridSizeX * GridOptions.GridSizeY;

	Result.X = Index % GridOptions.GridSizeX;
	Result.Y = (Index %  GridSizeSquared) / GridOptions.GridSizeX;
	Result.Y = Index / GridSizeSquared;

	return Result;
}

bool UGridBlueprintFunctionLibrary::TraceOnGrid(AGridManager* Grid, int32 StartIndex, int32 TargetIndex, ECollisionChannel CollisionChannel, float TraceHeight)
{
	const TMap<int32, FVector>& Locations = Grid->GetGridLocationsComponent()->GetLocations();

	FVector StartLoc = Locations[StartIndex];
	FVector EndLoc = Locations[TargetIndex];

	FVector Start = UGridBlueprintFunctionLibrary::ConvertFromGridToWorld(Grid, FVector(StartLoc.X, StartLoc.Y, TraceHeight));
	FVector End = UGridBlueprintFunctionLibrary::ConvertFromGridToWorld(Grid, FVector(EndLoc.X, EndLoc.Y, TraceHeight));

	FCollisionQueryParams TraceParams = FCollisionQueryParams();
	TraceParams.bTraceComplex = false;
	FHitResult Hit;

	bool IsHit = Grid->GetWorld()->LineTraceSingleByChannel(
		Hit,
		Start,
		End,
		CollisionChannel,
		TraceParams
	);

	return IsHit;
}

bool UGridBlueprintFunctionLibrary::CheckIfStraightAdjacent(AGridManager* Grid, int32 Index1, int32 Index2)
{
	const FGridOptions& GridOptions = Grid->GetGridOptions();

	int32 Val = FMath::Abs(Index1 - Index2);
	return Val == 1 || Val == GridOptions.GridSizeX;
}

int32 UGridBlueprintFunctionLibrary::ConvertLocationToIndex3D(AGridManager* Grid, FVector Vector, bool& bSuccess)
{
	const TMap<int32, FVector>& Locations = Grid->GetGridLocationsComponent()->GetLocations();
	const FProcedural& Procedural = Grid->GetProcedural();
	const FGridOptions& GridOptions = Grid->GetGridOptions();

	bSuccess = false;
	FVector CorrectedVector;
	int32 Result = UGridBlueprintFunctionLibrary::ConvertLocationToIndex2D(Grid, Vector, CorrectedVector);

	for (int32 i = 0; i < Grid->GetZSize(); i++)
	{
		int32 Index = GridOptions.GridSizeX * GridOptions.GridSizeY * i + Result;
		if (Locations.Contains(Index))
		{
			if (FMath::IsNearlyEqual(Locations[Index].Z, CorrectedVector.Z, Procedural.HeightBetweenLevels / 2.f))
			{
				bSuccess = UGridBlueprintFunctionLibrary::ConvertFromGridToWorld(Grid, Locations[Index]).Equals(Vector, Grid->GetActorScale3D().X * Grid->GetTileXSize());
				return Index;
			}
		}
	}

	return Result;
}

int32 UGridBlueprintFunctionLibrary::ConvertLocationToIndex2D(AGridManager* Grid, FVector Vector, FVector& CorrectedVector)
{
	const FGridOptions& GridOptions = Grid->GetGridOptions();

	CorrectedVector = UGridBlueprintFunctionLibrary::ConvertFromWorldToGrid(Grid, Vector);

	return FMath::FloorToInt((CorrectedVector.X + (Grid->GetTileXSize() / 2)) / Grid->GetTileXSize()) +
		(FMath::FloorToInt((CorrectedVector.Y + (Grid->GetTileXSize() / 2)) / Grid->GetTileXSize()) * GridOptions.GridSizeX);
}
