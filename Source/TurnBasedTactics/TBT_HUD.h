// Temp

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "TBT_HUD.generated.h"

/**
 * HUD used for Turn Based Tactics game.
 */
UCLASS()
class TURNBASEDTACTICS_API ATBT_HUD : public AHUD
{
	GENERATED_UCLASS_BODY()
	
};
