// Temp

#pragma once

#include "CoreMinimal.h"
#include "GridActor.h"
#include "GridManagerTypes.h"
#include "GA_Tile.generated.h"

/**
 * The class on which all tiles are based. Handles grid positioning and snapping and interacts with Grid Manager for instantiating meshes.
 */
UCLASS()
class TURNBASEDTACTICS_API AGA_Tile : public AGridActor
{
	GENERATED_UCLASS_BODY()

	/** Gets cusom tile edges. */
	FORCEINLINE	const FEdges& GetCustomEdges() const
	{
		return CustomEdges;
	}

	/** Gets tile edge costs. */
	FORCEINLINE const FEdges& GetEdgeCosts() const
	{
		return EdgeCosts;
	}

	/** Gets indexes of all edges of this tile. */
	FORCEINLINE const TArray<int32>& GetEdges() const
	{
		return Edges;
	}

	/** Gets blocked corner indexes. */
	FORCEINLINE const TArray<FTwoInts>& GetBlockedCornerIndexes() const
	{
		return BlockedCornerIndexes;
	}

	/** Gets simple cost for this tile. */
	FORCEINLINE int32 GetSimpleCost() const
	{
		return SimpleCost;
	}

protected:
	/** Any edges added to this array will be added as edges to this tile during setup. */
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Default")
	FEdges CustomEdges;

	/**  */
	UPROPERTY(BlueprintReadOnly, Category = "Default")
	FEdges EdgeCosts;

	/** Indexes of all edges of this tile */
	UPROPERTY(BlueprintReadOnly, Category = "Default")
	TArray<int32> Edges;

	/** Holds the indexes of tiles on either side of a corner. Marked so edges between these will be removed if the EdgeCost of the corner between these is set to -1. */
	UPROPERTY(BlueprintReadOnly, Category = "Default")
	TArray<FTwoInts> BlockedCornerIndexes;

	/** If any of the simple pathfinding types are used, this value determines the universal cost of entering this tile, no matter from what direction. 0 = impassable. */
	UPROPERTY(BlueprintReadOnly, Category = "Default")
	int32 SimpleCost;
};
