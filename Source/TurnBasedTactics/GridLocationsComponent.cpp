// Temp

#include "GridLocationsComponent.h"
#include "CommonTypes.h"
#include "GridManager.h"
#include "GridBlueprintFunctionLibrary.h"

UGridLocationsComponent::UGridLocationsComponent()
{
}

void UGridLocationsComponent::Setup(AGridManager* _Grid)
{
	check(_Grid);

	Grid = _Grid;
	GridSizeSquared = Grid->GetGridOptions().GridSizeX * Grid->GetGridOptions().GridSizeY;
}

void UGridLocationsComponent::ActivateGridLocations()
{

}

void UGridLocationsComponent::Create(bool& bGridHasHoles, int32& GridSizeZ)
{
	check(Grid);

	const FProcedural& Procedural = Grid->GetProcedural();
	const FGridOptions& GridOptions = Grid->GetGridOptions();

	Locations.Empty();
	bGridHasHoles = false;
	GridSizeZ = 1;

	// Create the vector grids that form the basis of moving between grid array indexes and world locations
	if (Procedural.Heightmap == EHeight::H_False && GridOptions.bCollisionPlaneWalkable)
	{
		for (int32 i = 0; i < GridSizeSquared; i++)
		{
			Locations.Add(i, ConvertIndexToLocationSquareGrid(i));
		}
		return;
	}

	for (int32 i = 0; i < GridSizeSquared; i++)
	{
		CreateLocationsAndHeightmap(i, ConvertIndexToLocationSquareGrid(i), bGridHasHoles, GridSizeZ);
	}
}

FVector UGridLocationsComponent::ConvertIndexToLocationSquareGrid(int32 Index)
{
	check(Grid);

	const FGridOptions& GridOptions = Grid->GetGridOptions();

	FVector Result(
		(Index % GridOptions.GridSizeX) * Grid->GetTileXSize(),
		(Index / GridOptions.GridSizeX - (GridOptions.GridSizeX * (Index / (GridOptions.GridSizeX * GridOptions.GridSizeY)))) * Grid->GetTileYSize(),
		Index / (GridOptions.GridSizeX * GridOptions.GridSizeY) * 200.f
	);

	return Result;
}

void UGridLocationsComponent::Add(int32 Index, FVector Location)
{
	Locations.Add(Index, Location);
}

bool UGridLocationsComponent::Remove(int32 Index)
{
	if (Locations.Contains(Index))
	{
		Locations.Remove(Index);
		return true;
	}

	return false;
}

void UGridLocationsComponent::CreateLocationsAndHeightmap(int32 GridIndex, FVector Location, bool& bGridHasHoles, int32& GridSizeZ)
{
	check(Grid);

	const FProcedural& Procedural = Grid->GetProcedural();

	FVector Start = UGridBlueprintFunctionLibrary::ConvertFromGridToWorld(Grid, FVector(Location.X, Location.Y, Procedural.MaxGridHeight));
	FVector End = UGridBlueprintFunctionLibrary::ConvertFromGridToWorld(Grid, FVector(Location.X, Location.Y, Procedural.MinGridHeight - 1.f));

	FCollisionQueryParams TraceParams = FCollisionQueryParams();
	TraceParams.bTraceComplex = false;
	FHitResult Hit;

	bool IsHit = GetWorld()->LineTraceSingleByChannel(
		Hit,
		Start,
		End,
		COLLISION_PATH,
		TraceParams
	);

	FVector Impact = Hit.Location - UGridBlueprintFunctionLibrary::ConvertFromGridToWorld(Grid, FVector(Location.X, Location.Y, 0.f));
	float ImpactAtHeight = Impact.Size();

	if (IsHit)
	{
		switch (Procedural.Heightmap)
		{
		case EHeight::H_False:
			Locations.Add(GridIndex, Location);
			break;

		case EHeight::H_OneLevel:
			Locations.Add(GridIndex, FVector(Location.X, Location.Y, Location.Z + ImpactAtHeight));
			break;

		case EHeight::H_Multilevel:
			if (ImpactAtHeight - Procedural.HeightBetweenLevels > Procedural.MinGridHeight)
			{
				GridSizeZ = FMath::Max(
					GridSizeZ,
					AddMultilevelGridAtIndex(GridIndex, Hit.Location)
				);
			}
			else
			{
				Locations.Add(GridIndex, FVector(Location.X, Location.Y, Location.Z + ImpactAtHeight));
			}
			break;
		}
	}
	else
	{
		bGridHasHoles = true;
	}
}

int32 UGridLocationsComponent::AddMultilevelGridAtIndex(int32 Index, FVector StartLocation)
{
	// Checks below the initial location hit by Path Trace during startup, searching for platforms with enough space above them that they should be walkable without units clipping through the roof. Keep searching until all potential platforms are found.
	TArray<FVector> Loc = FindOverlappingTileLocations(Index, StartLocation);

	for (int32 i = 0; i < Loc.Num(); i++)
	{
		FVector Location = Loc[i];
		Locations.Add(
			Index + (Loc.Num() - 1 - i) * GridSizeSquared,
			UGridBlueprintFunctionLibrary::ConvertFromWorldToGrid(Grid, Location)
		);
	}

	return Loc.Num();
}

TArray<FVector> UGridLocationsComponent::FindOverlappingTileLocations(int32 Index, FVector StartLocation)
{
	bool EnoughSpace = true;

	const FProcedural& Procedural = Grid->GetProcedural();

	TArray<FVector> LocalLocations;
	FVector LocalStartLocation = StartLocation;
	FVector LocalEndLocation;

	do
	{
		if (EnoughSpace)
		{
			// Store the initial hit (top) location
			LocalLocations.Add(LocalStartLocation);

			if (LocalLocations.Num() >= Procedural.MaxLevels - 1)
			{
				return LocalLocations;
			}
		}

		LocalStartLocation -= Grid->GetActorUpVector() * Procedural.HeightBetweenLevels;

		if (UGridBlueprintFunctionLibrary::ConvertFromWorldToGrid(Grid, LocalStartLocation).Z - 1 > Procedural.MinGridHeight)
		{
			LocalEndLocation = UGridBlueprintFunctionLibrary::ConvertFromGridToWorld(Grid, FVector(StartLocation.X, StartLocation.Y, Procedural.MinGridHeight - 1));

			FCollisionQueryParams TraceParams1 = FCollisionQueryParams();
			TraceParams1.bTraceComplex = false;
			FHitResult Hit1;

			bool IsHit1 = GetWorld()->LineTraceSingleByChannel(
				Hit1,
				LocalStartLocation,
				LocalEndLocation,
				COLLISION_PATH,
				TraceParams1
			);

			if (IsHit1)
			{
				LocalStartLocation = Hit1.Location;

				FVector Start = LocalStartLocation + Grid->GetActorUpVector() * Grid->GetActorScale3D();
				FVector End = LocalStartLocation + Grid->GetActorUpVector() * Grid->GetActorScale3D() * Procedural.HeightBetweenLevels;

				// If the trace is a hit trace upwards for [Tile Bounds Z] unreal units.
				// If it does not hit there is a big enough distance between platforms
				// that the lowest one is walkable. If so, store its location and continue
				// tracing downwards for further platforms.
				FCollisionQueryParams TraceParams2 = FCollisionQueryParams();
				TraceParams2.bTraceComplex = false;
				FHitResult Hit2;

				bool IsHit2 = GetWorld()->LineTraceSingleByChannel(
					Hit2,
					Start,
					End,
					COLLISION_PATH,
					TraceParams2
				);

				if (IsHit2)
				{
					EnoughSpace = false;
				}
				else
				{
					EnoughSpace = true;
				}
			}
			else
			{
				return LocalLocations;
			}
		}
		else
		{
			return LocalLocations;
		}
	}
	while (true);
}
