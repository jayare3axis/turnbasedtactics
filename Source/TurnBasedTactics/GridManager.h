// Temp

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GridManagerTypes.h"
#include "Components/BoxComponent.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "GridLocationsComponent.h"
#include "GridEdgesComponent.h"
#include "PathfinderComponent.h"
#include "GridManager.generated.h"

// Forward
class AUnit;
class ATBT_GameState;
class ATurnManager;
class AActionManager;

UCLASS()
class TURNBASEDTACTICS_API AGridManager : public AActor
{
	GENERATED_UCLASS_BODY()
	
public:
	/** Activates Grid Manager */
	void ActivateGridManager();

	/** Gets grid options. */
	FORCEINLINE const FGridOptions& GetGridOptions() const
	{
		return GridOptions;
	}

	/** Gets procedural. */
	FORCEINLINE const FProcedural& GetProcedural() const
	{
		return Procedural;
	}

	/** Gets tile X size */
	FORCEINLINE float GetTileXSize() const
	{
		return TileXSize;
	}

	/** Gets tile Y size */
	FORCEINLINE float GetTileYSize() const
	{
		return TileYSize;
	}

	/** Gets grid Z size */
	FORCEINLINE float GetZSize() const
	{
		return GridSizeZ;
	}

	/** Returns true if grid has holes. */
	FORCEINLINE bool HasHoles() const
	{
		return bGridHasHoles;
	}

	/** Returns grid units. */
	FORCEINLINE const TMap<int32, AUnit*>& GetGridUnits() const
	{
		return GridUnits;
	}

	/** Gets grid locations component. */
	FORCEINLINE UGridLocationsComponent* GetGridLocationsComponent() const
	{
		return GridLocations;
	}

	/** Gets grid edges component. */
	FORCEINLINE UGridEdgesComponent* GetGridEdgesComponent() const
	{
		return GridEdges;
	}

	/** Gets pathfinder component. */
	FORCEINLINE UPathfinderComponent* GetPathfinderComponent() const
	{
		return Pathfinder;
	}
protected:
	/* AActor */
	virtual void OnConstruction(const FTransform& Transform) override;

#pragma region
	/** Initializes plane component */
	void InitCollisionPlane();

	/** Initializes heightmap box component */
	void InitHeightmapBox();

	/** Prevent the user from choosing incompatible combinations of public variables */
	void PreventIncompatibleVariableVariations();

	/** Setup the default mesh and tile size */
	void SetupScaleAndDefaultTiles();

	/** Scale and position the collision plane. This makes individual collision volumes unecessary for flat grids, and makes it possible to place meshes in the viewport if the default tile is invisible */
	void SetupCollisionPlane();

	/** Scale and Position HeightmapBox. Is used to show the the maximum and minimum Z locations on which heightmap tracing is used */
	void SetupHeightmapBox();

	/** Spawns Tile Instnced meshes in a grid pattern based on grid size and type */
	void SpawnTileGrid();

	/** Generates grids and realigns units. Is by default run in construction script if PregenerateGameplayGrids is true. Displays tile edges and indexes if specified. If changes are made to the grid by placing new tiles/terrain etc. this function should be re-run for the edge array to update appropriately */
	void PregenerateGameplayGrids();

	/** Experimental work-in-progress. Uses predefined search patterns to find the max size of unit that can occupy each tile. Does currently not support hexagonal or multi-level grids. */
	//void MakeBigUnitArray();

	/** Scales the collision box and heightmap box to cover the entire grid */
	FLocationAndSize2D SetCollisionPlaneAndHeightmapBoxScaleAndLocation();

	/** Individual collision for each tile is disabled at runtime for performance reasons. the collision plane does the same thing cheaper. */
	void SetupGridCollision();
#pragma endregion Startup

#pragma region
	/** Pregenerate gameplay grids must be enabled for this function to function. Displays the grid indexes of all tiles. */
	UFUNCTION(BlueprintImplementableEvent)
	void DisplayTileIndexes();

	/** Pregenerate gameplay grids must be enabled for this function to function. Displays the edges and edge costs of all tiles. */
	UFUNCTION(BlueprintImplementableEvent)
	void DisplayTileEdges();

	/** Turns actors with "Instantiate" set to true to instances static meshes in the viewport, which can greatly increase performance */
	void ReplaceActorMeshesWithInstances();

	/** Reverses the process done by checking instantiate */
	void ConvertInstancesToActors();
#pragma endregion Miscellaneous

	/** Standard grid options */
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Config")
	FGridOptions GridOptions;

	/** Grid options used for procedural generation */
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Config")
	FProcedural Procedural;

	/** Holds static meshes used by the grid manager. */
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Config")
	FStaticMeshes StaticMeshes;

	/** GridSizeX * GridSizeY */
	UPROPERTY(BlueprintReadOnly, Category = "Miscellaneous")
	int32 GridSizeSquared;

	/** Grid size for multilevel grids */
	UPROPERTY(BlueprintReadOnly, Category = "Miscellaneous")
	int32 GridSizeZ;

	/** Stores tile width */
	UPROPERTY(BlueprintReadOnly, Category = "Miscellaneous")
	int32 TileXSize;

	/** Stores tile width */
	UPROPERTY(BlueprintReadOnly, Category = "Miscellaneous")
	int32 TileYSize;

	/** Tells if grid has holes */
	UPROPERTY(BlueprintReadOnly, Category = "Miscellaneous")
	bool bGridHasHoles;

	/** True when grid setup is done. */
	UPROPERTY(BlueprintReadOnly, Replicated, Category = "Miscellaneous")
	bool bGridSetupDone;

	/** Holds the grid index location of all units */
	UPROPERTY(BlueprintReadOnly, Category = "GridArrays")
	TMap<int32, AUnit*> GridUnits;

	UPROPERTY(BlueprintReadOnly)
	USceneComponent* Scene;

	UPROPERTY(BlueprintReadOnly)
	UStaticMeshComponent* CollisionPlane;

	UPROPERTY(BlueprintReadOnly)
	UBoxComponent* HeightmapBox;

	UPROPERTY(BlueprintReadOnly)
	UInstancedStaticMeshComponent* DefaultTile;

	UPROPERTY(BlueprintReadOnly)
	UGridLocationsComponent* GridLocations;

	UPROPERTY(BlueprintReadOnly)
	UGridEdgesComponent* GridEdges;

	UPROPERTY(BlueprintReadOnly)
	UPathfinderComponent* Pathfinder;

#pragma region
	UPROPERTY(BlueprintReadOnly, Category = "References")
	ATBT_GameState* GameStateRef;

	UPROPERTY(BlueprintReadOnly, Category = "References")
	ATurnManager* TurnManager;

	UPROPERTY(BlueprintReadOnly, Category = "References")
	AActionManager* ActionManager;
#pragma endregion References

private:
	UPROPERTY()
	bool bInstantiating;
};
