// Temp

#include "DelayedTimerDelegate.h"

UDelayedTimerDelegate::UDelayedTimerDelegate()
{
}

void UDelayedTimerDelegate::DelayForTicks(int32 _Ticks)
{
	check(_Ticks > 1);
	check(WorldContextObject);

	Ticks = _Ticks;

	WorldContextObject->GetWorldTimerManager().SetTimerForNextTick(DelayedTickDelegate);
}

void UDelayedTimerDelegate::ProcessDelayedTick()
{
	Ticks--;

	WorldContextObject->GetWorldTimerManager().SetTimerForNextTick(
		Ticks == 1 ? 
		TimerDelegate :
		DelayedTickDelegate
	);
}
