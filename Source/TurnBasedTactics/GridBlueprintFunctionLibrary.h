// Temp

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "GridManager.h"
#include "GridBlueprintFunctionLibrary.generated.h"

/**
 * Utilities for GridManager
 */
UCLASS()
class TURNBASEDTACTICS_API UGridBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	/** Gets a grid location and converts it into an actual world location, taking the Grid Managers transform into account */
	UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldContextObject"), Category = "Grid")
	static FVector ConvertFromGridToWorld(AGridManager* Grid, FVector Location);

	/** Takes an in-game location and converts it into a grid location (a relative location to grid index 0) */
	UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldContextObject"), Category = "Grid")
	static FVector ConvertFromWorldToGrid(AGridManager* Grid, FVector Location);

	/** Converts a grid index to a struct specifying grid position as X, Y and Z instead of a continous rising number. */
	UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldContextObject"), Category = "Grid")
	static FGridIndex ConvertGridIndexToStruct(AGridManager* Grid, int32 Index);

	/** fires a line trace between two tiles, noting if it is a hit */
	UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldContextObject"), Category = "Grid")
	static bool TraceOnGrid(AGridManager* Grid, int32 StartIndex, int32 TargetIndex, ECollisionChannel CollisionChannel, float TraceHeight);

	/** Gets a location and finds the closest corresponding location. Takes overlapping tiles into account */
	UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldContextObject"), Category = "Grid")
	static int32 ConvertLocationToIndex3D(AGridManager* Grid, FVector Vector, bool& bSuccess);

	/** Gets a location and finds the closest corresponding location. Does not take overlapping tiles into account */
	UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldContextObject"), Category = "Grid")
	static int32 ConvertLocationToIndex2D(AGridManager* Grid, FVector Vector, FVector& CorrectedVector);

	/** Checks if two adjacent tiles are not diagonally adjacent */
	UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldContextObject"), Category = "Grid")
	static bool CheckIfStraightAdjacent(AGridManager* Grid, int32 Index1, int32 Index2);
	
};
