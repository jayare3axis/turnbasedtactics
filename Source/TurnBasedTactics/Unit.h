// Temp

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "UnitsTypes.h"
#include "Unit.generated.h"

/**
* The parent class for all Unit actors. Handles everything except animations, including movement and attacking, taking damage and destroying pawns. Also handles grid snapping and the health bar. To create a new pawn it is recommended to create a duplicate of any of the example pawns instead of creating a child of Unit_Parent as these also hold animation nodes.
*/
UCLASS()
class TURNBASEDTACTICS_API AUnit : public ACharacter
{
	GENERATED_UCLASS_BODY()

	/** Gets attributes. */
	const FAttributes& GetAttributes() const;

	/** Gets movement options. */
	const FMovementOptions& GetMovementOptions() const;

protected:
	/** Attributes */
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Config")
	FAttributes Attribures;

	/** Movement options */
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Config")
	FMovementOptions MovementOptions;
};
