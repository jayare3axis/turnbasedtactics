// Temp

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "GameplayTypes.h"
#include "TBT_GameState.generated.h"

// Forward
class AGridManager;
class ATurnManager;
class AActionManager;

/**
 * Along with basic game state duties, serves as a common object to call on to get references to important singleton objects.
 */
UCLASS()
class TURNBASEDTACTICS_API ATBT_GameState : public AGameStateBase
{
	GENERATED_UCLASS_BODY()

public:
	/** Spawns Turn and Action managers. Adds references to these and the grid manager to the game state. The game state is used as a singleton object where other objeccts can easily access these references. */
	void SetupCoreActors();

	/** Sets turn state to a new value. */
	void SetTurnState(ETurnState NewTurnState);

	/** Gets turn manager/ */
	FORCEINLINE ATurnManager* GetTurnManager() const
	{
		return TurnManager;
	}

	/** Gets action manager/ */
	FORCEINLINE AActionManager* GetActionManager() const
	{
		return ActionManager;
	}
	
protected:
	/** Spawns Turn Manager of this class. */
	UPROPERTY(EditDefaultsOnly, Category = "Default")
	TSubclassOf<ATurnManager> TurnManagerClass;

	/** Spawns Action Manager of this class */
	UPROPERTY(EditDefaultsOnly, Category = "Default")
	TSubclassOf<AActionManager> ActionManagerClass;

	/** Holds current turn state. */
	UPROPERTY(BlueprintReadOnly, Replicated, Category = "Default")
	ETurnState TurnState;

#pragma region
	UPROPERTY(BlueprintReadOnly, Replicated, Category = "References")
	AGridManager* GridManager;
	
	UPROPERTY(BlueprintReadOnly, Replicated, Category = "References")
	ATurnManager* TurnManager;

	UPROPERTY(BlueprintReadOnly, Replicated, Category = "References")
	AActionManager* ActionManager;
#pragma endregion References
};
