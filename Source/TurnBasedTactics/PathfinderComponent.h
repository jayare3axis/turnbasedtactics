// Temp

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GridManagerTypes.h"
#include "UnitsTypes.h"
#include "PathfinderComponent.generated.h"

// Forward
class AGridManager;
class ATurnManager;

/**
* Handles pathfinding.
*/
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TURNBASEDTACTICS_API UPathfinderComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UPathfinderComponent();

	/** Sets up parameters for pathfinding. */
	void Setup(AGridManager* Grid);

	/** Activates the component. */
	void ActivatePathfinder();

	/** Searches outwards from specified indexes to the specified move range using the grid edges. Returns all tiles reachable given these parameters and any units found in the search. */
	UFUNCTION(BlueprintCallable, Category="Pathfinding")
	void Run(int32 StartIndex, int32 MoveRange, EPathfindingType Type, FPathfindingResult& Result);

protected:
	/** Chooses and runs the specified Search And Add Adjacent Tiles function based on the pathfinding type specified. */
	FPathfindingMap ChooseAndRunPathfindingSearchStep(EPathfindingType Type, int32 SearchStep, FPathfindingMap& PathsMap, TSet<int32>& ReachableUnits, int32 Move, FPathfindingMap& OpenListTiles, FPathfindingMap& DelayedSearchTiles, FPathfindingMap& ContinuePathTiles);

	/** All tiles pending search from the last search step are checked. If they can be moved to they are added to the appropriate arrays and search continues from them the next search step. */
	FPathfindingMap SearchAndAddAdjacentTiles(int32 SearchStep, FPathfindingMap& PathsMap, TSet<int32>& ReachableUnits, int32 Move, FPathfindingMap& OpenListTiles, FPathfindingMap& DelayedSearchTiles, FPathfindingMap& ContinuePathTiles);

	/** Like SearchAndAddAdjacentTiles, but pathfinding continues past tiles occupied by units of the same faction as the active unit */
	FPathfindingMap SearchAndAddAdjacentTilesPassThroughFriendly(int32 SearchStep, FPathfindingMap& PathsMap, TSet<int32>& ReachableUnits, int32 Move, FPathfindingMap& OpenListTiles, FPathfindingMap& DelayedSearchTiles, FPathfindingMap& ContinuePathTiles);

	/** Like SearchAndAddAdjacentTiles, but treats all movement costs in the edge array as 1 */
	FPathfindingMap SearchAndAddAdjacentTilesIgnoreCost(int32 SearchStep, FPathfindingMap& PathsMap, TSet<int32>& ReachableUnits, int32 Move, FPathfindingMap& OpenListTiles, FPathfindingMap& DelayedSearchTiles, FPathfindingMap& ContinuePathTiles);

	/** Like SearchAndAddAdjacentTiles, but used for big units */
	FPathfindingMap SearchAndAddAdjacentTilesBig(int32 SearchStep, FPathfindingMap& PathsMap, TSet<int32>& ReachableUnits, int32 Move, FPathfindingMap& OpenListTiles, FPathfindingMap& DelayedSearchTiles, FPathfindingMap& ContinuePathTiles);

	/** Like SearchAndAddAdjacentTiles, but does not allow diagonal movement. Only useful if the grid used normally allows for diagonal movement. */
	FPathfindingMap SearchAndAddAdjacentTilesNoDiagonals(int32 SearchStep, FPathfindingMap& PathsMap, TSet<int32>& ReachableUnits, int32 Move, FPathfindingMap& OpenListTiles, FPathfindingMap& DelayedSearchTiles, FPathfindingMap& ContinuePathTiles);

	/** Identical to SearchAndAddAdjacentTiles. Is included as an easy starting point to create a new custom pathfinding type. If you want to add more types of pathfinding, duplicate this function, add it to ChooseAndRunPathfindingSearchStep and expand the EPathfinding enum as needed. */
	FPathfindingMap SearchAndAddAdjacentTilesSimpleMixed(int32 SearchStep, FPathfindingMap& PathsMap, TSet<int32>& ReachableUnits, int32 Move, FPathfindingMap& OpenListTiles, FPathfindingMap& DelayedSearchTiles, FPathfindingMap& ContinuePathTiles);

	/** Identical to SearchAndAddAdjacentTiles. Is included as an easy starting point to create a new custom pathfinding type. If you want to add more types of pathfinding, duplicate this function, add it to ChooseAndRunPathfindingSearchStep and expand the EPathfinding enum as needed. */
	FPathfindingMap SearchAndAddAdjacentTilesSimplePure(int32 SearchStep, FPathfindingMap& PathsMap, TSet<int32>& ReachableUnits, int32 Move, FPathfindingMap& OpenListTiles, FPathfindingMap& DelayedSearchTiles, FPathfindingMap& ContinuePathTiles);

	/** Like SearchAndAddAdjacentTiles, but treats all movement costs in the edge array as 1 */
	FPathfindingMap SearchAndAddAdjacentTilesSimpleIgnoreCost(int32 SearchStep, FPathfindingMap& PathsMap, TSet<int32>& ReachableUnits, int32 Move, FPathfindingMap& OpenListTiles, FPathfindingMap& DelayedSearchTiles, FPathfindingMap& ContinuePathTiles);

#pragma region
	/** Finds all units that are visible from any tile index currently in the Can Move To Array. */
	TSet<int32> KeepTargetsInSightFromMoveArray(TSet<int32> TargetIndexes, int32 Range, int32 MinRange, bool DiamondShapedVisibility, bool bAvoidTileOccupiedByCurrentUnit, FPathfindingMap PathsMap);
#pragma endregion Visibility

	AGridManager* Grid;
	int32 GridSizeSquared;

	UPROPERTY()
	TSet<int32> SearchedTiles;

#pragma region
	UPROPERTY()
	ATurnManager* TurnManager;
#pragma endregion References
};
