// Temp

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GridManagerTypes.h"
#include "GridLocationsComponent.generated.h"

// Forward
class AGridManager;

/**
* Handles creating, altering and storing locations for the grid.
*/
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TURNBASEDTACTICS_API UGridLocationsComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UGridLocationsComponent();

	/** Sets up parameters for location generation. */
	void Setup(AGridManager* Grid);

	/** Activates the component. */
	void ActivateGridLocations();

	/** Create the vector grids that form the basis of moving between grid array indexes and world locations. */
	void Create(bool& bGridHasHoles, int32& GridSizeZ);

	/** Used when first generating the grid. Do not use on runtime. Instead get the index from the Vector Field Array, which is generated using this function. */
	FVector ConvertIndexToLocationSquareGrid(int32 Index);

	/** Gets the grid locations. */
	FORCEINLINE const TMap<int32, FVector>& GetLocations() const
	{
		return Locations;
	}

	/** Adds new location at given index. */
	UFUNCTION(BlueprintCallable, Category = "Default")
	void Add(int32 Index, FVector Location);

	/** Removes location from the grid and returns true on success. */
	UFUNCTION(BlueprintCallable, Category = "Default")
	bool Remove(int32 Index);

protected:
	/** At startup, generates a map of all grid tile locations. Heightmap = false: flat grid, heightmap = One Level: Uses line traces to find highest points within defined boundries that blocks PathTrace. Heightmap = multilevel: Keeps tracing after first hit, adding more levels to the locations map when found until outside MinGridHeight. */
	void CreateLocationsAndHeightmap(int32 GridIndex, FVector Location, bool& bGridHasHoles, int32& GridSizeZ);

	/** Checks below the initial location hit by Path Trace during startup, searching for platforms with enough space above them that they should be walkable without units clipping through the roof. Keep searching until all potential platforms are found.*/
	int32 AddMultilevelGridAtIndex(int32 Index, FVector StartLocation);

	/** Traces from the sky to the ground. If PathTrace hits, traces upwards from hit location. If the new trace does not hit anything before it has traveled HeightBetweenLevels this location is added to the output array. A new trace is then fired at HeightBetweenLevels under the first hit location and the process continues until the trace reaches MinGridHeight or the number of levels exeeds MaxLevels */
	TArray<FVector> FindOverlappingTileLocations(int32 Index, FVector StartLocation);

	/** Holds the location of all tile indexes. To instead go from location to index use the Vector To Index macro. */
	UPROPERTY(BlueprintReadOnly, Category = "Default")
	TMap<int32, FVector> Locations;

	AGridManager* Grid;
	int32 GridSizeSquared;
};
