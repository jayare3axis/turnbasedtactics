// Temp

#include "GridEdgesComponent.h"
#include "GridManager.h"
#include "GA_Tile.h"
#include "TurnManager.h"
#include "CommonTypes.h"
#include "GridBlueprintFunctionLibrary.h"
#include "TBT_GameState.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

UGridEdgesComponent::UGridEdgesComponent()
{
}

void UGridEdgesComponent::Setup(AGridManager* _Grid)
{
	check(_Grid);

	Grid = _Grid;
	GridSizeSquared = Grid->GetGridOptions().GridSizeX * Grid->GetGridOptions().GridSizeY;
}

void UGridEdgesComponent::ActivateGridEdges()
{
	TurnManager = Cast<ATBT_GameState>( UGameplayStatics::GetGameState(GetWorld()) )->GetTurnManager();
}

void UGridEdgesComponent::Create()
{
	check(Grid);

	Edges.Empty();

	CreateBaseEdges();
	SetEdgesBasedOnTerrain();
	AddAllTileActorEdges();
	RemoveUnreachableTiles();
}

void UGridEdgesComponent::CreateSimpleCosts()
{
	const TMap<int32, FVector>& Locations = Grid->GetGridLocationsComponent()->GetLocations();

	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AGA_Tile::StaticClass(), FoundActors);
	for (AActor* Actor : FoundActors)
	{
		AGA_Tile* TileActor = Cast<AGA_Tile>(Actor);
		SimpleCosts.Add(TileActor->GetGridIndex(), TileActor->GetSimpleCost());
	}

	for (auto& Pair : Locations)
	{
		if (!SimpleCosts.Contains(Pair.Key))
		{
			SimpleCosts.Add(Pair.Key, 1);
		}
	}
}

void UGridEdgesComponent::AddTileEdge(int32 TileIndex, int32 Edge, int32 Cost)
{
	if (Edges[TileIndex].Edges.Contains(Edge))
	{
		Edges[TileIndex].Edges[Edge] = Cost;
	}
	else
	{
		Edges[TileIndex].Edges.Add(Edge, Cost);
	}
}

void UGridEdgesComponent::RemoveTileEdge(int32 TileIndex, int32 Edge)
{
	Edges[TileIndex].Edges.Remove(Edge);
}

void UGridEdgesComponent::RemoveTileEdgeBothWays(int32 Index1, int32 Index2)
{
	RemoveTileEdge(Index1, Index2);
	RemoveTileEdge(Index2, Index1);
}

void UGridEdgesComponent::SetEdgeCost(int32 TileIndex, int32 Edge, int32 NewCost)
{
	if (Edges[TileIndex].Edges.Contains(Edge))
	{
		Edges[TileIndex].Edges[Edge] = NewCost;
	}
}

void UGridEdgesComponent::CreateBaseEdges()
{
	const FGridOptions& GridOptions = Grid->GetGridOptions();

	// Populates the Edge Array so that all tiles are connected to their neighboring tiles with a movement cost of 1 on a square grid	
	for (int32 i = 0; i < GridSizeSquared; i++)
	{
		FEdges E;

		// North
		E.Edges.Add(i - GridOptions.GridSizeX, 1);
		// East
		E.Edges.Add(i + 1, 1);
		// South
		E.Edges.Add(i + GridOptions.GridSizeX, 1);
		// West
		E.Edges.Add(i - 1, 1);
		if (GridOptions.bDiagonalMovement)
		{
			// North East
			E.Edges.Add(i - GridOptions.GridSizeX + 1, 1);
			// South East
			E.Edges.Add(i + GridOptions.GridSizeX + 1, 1);
			// South West
			E.Edges.Add(i + GridOptions.GridSizeX - 1, 1);
			// North West
			E.Edges.Add(i - GridOptions.GridSizeX - 1, 1);
		}

		Edges.Add(E);
	}

	RemoveEdgesOnGridBorders();
}

void UGridEdgesComponent::RemoveEdgesOnGridBorders()
{
	const FGridOptions& GridOptions = Grid->GetGridOptions();

	TMultiMap<int32, int32> EdgesToRemove;

	// Remove south edges on north border
	for (int32 i = 0; i <  GridOptions.GridSizeX; i++)
	{
		for (auto& Pair : Edges[i].Edges)
		{
			if (Pair.Key < i - 1)
			{
				EdgesToRemove.Add(i, Pair.Key);
			}
		}
	}
	// Remove north edges on south border
	for (int32 i = GridSizeSquared - GridOptions.GridSizeX; i < GridSizeSquared; i++)
	{
		for (auto& Pair : Edges[i].Edges)
		{
			if (Pair.Key > i + 1)
			{
				EdgesToRemove.Add(i, Pair.Key);
			}
		}
	}
	// Remove west edges on east border
	for (int32 i = 1; i <= GridOptions.GridSizeY; i++)
	{
		int32 Index = i * GridOptions.GridSizeX - 1;
		for (auto& Pair : Edges[Index].Edges)
		{
			FGridIndex Struct = UGridBlueprintFunctionLibrary::ConvertGridIndexToStruct(Grid, Pair.Key);
			if (Struct.X == 0)
			{
				EdgesToRemove.Add(Index, Pair.Key);
			}
		}
	}
	// Remove east edges on west border
	for (int32 i = 0; i < GridOptions.GridSizeY; i++)
	{
		int32 Index = i * GridOptions.GridSizeX;
		for (auto& Pair : Edges[Index].Edges)
		{
			FGridIndex Struct = UGridBlueprintFunctionLibrary::ConvertGridIndexToStruct(Grid, Pair.Key);
			if (Struct.X == GridOptions.GridSizeX - 1)
			{
				EdgesToRemove.Add(Index, Pair.Key);
			}
		}
	}
	EdgesToRemove.Add(0, -1);
	EdgesToRemove.Add(GridOptions.GridSizeX, -1);

	for (auto& Pair : EdgesToRemove)
	{
		RemoveTileEdge(Pair.Key, Pair.Value);
	}
}

void UGridEdgesComponent::SetEdgesBasedOnTerrain()
{
	const TMap<int32, FVector>& Locations = Grid->GetGridLocationsComponent()->GetLocations();
	const FProcedural& Procedural = Grid->GetProcedural();
	const FGridOptions& GridOptions = Grid->GetGridOptions();

	BaseEdges = Edges;

	if (Procedural.Heightmap != EHeight::H_Multilevel &&
		!(Procedural.Heightmap == EHeight::H_OneLevel && Procedural.bAutoEdgeCostsBasedOnHeight) &&
		!Grid->HasHoles() &&
		!Procedural.bTraceForWalls)
	{
		return;
	}

	Edges.Empty();
	Edges.SetNum(GridSizeSquared * Grid->GetZSize());

	int32 EdgeCheckLimit = (Grid->GetGridOptions().bDiagonalMovement) ? 4 : 8;

	for (int32 i = GridSizeSquared * Grid->GetZSize() - 1; i >= 0; i--)
	{
		if (Locations.Contains(i))
		{
			int32 Index = 0;
			for (auto& Pair : BaseEdges[i % GridSizeSquared].Edges)
			{
				if (Index < EdgeCheckLimit)
				{
					for (int32 j = Grid->GetZSize() - 1; j >= 0; j--)
					{
						int32 EdgeCost;
						bool AlreadyContains;
						int32 EdgeIndex = j * GridSizeSquared + Pair.Key;
						if (CheckIfValidEdgeCandidate(EdgeIndex, i, Procedural.bTraceForWalls, AlreadyContains, EdgeCost))
						{
							if (j > i / GridSizeSquared)
							{
								break;
							}
							else
							{
								Edges[EdgeIndex].Edges.Add(i, EdgeCost);
								Edges[i].Edges.Add(EdgeIndex, EdgeCost);
								break;
							}
						}
						else if (AlreadyContains)
						{
							break;
						}
					}
				}
				Index++;
			}
		}
	}

	if (GridOptions.bDiagonalMovement)
	{
		for (int32 i = GridSizeSquared * Grid->GetZSize() - 1; i >= 0; i--)
		{
			if (Locations.Contains(i))
			{
				int32 Index = 0;
				for (auto& Pair : BaseEdges[i % GridSizeSquared].Edges)
				{
					if (Index >= EdgeCheckLimit)
					{
						for (int32 j = Grid->GetZSize() - 1; j >= 0; j--)
						{
							int32 EdgeCost;
							bool AlreadyContains;
							int32 EdgeIndex = j * GridSizeSquared + Pair.Key;
							if (CheckIfValidEdgeCandidate(EdgeIndex, i, Procedural.bTraceForWalls, AlreadyContains, EdgeCost))
							{
								if (j > i / GridSizeSquared)
								{
									break;
								}
								else
								{
									Edges[EdgeIndex].Edges.Add(i, EdgeCost);
									Edges[i].Edges.Add(EdgeIndex, EdgeCost);
									break;
								}
							}
							else if (AlreadyContains)
							{
								break;
							}
						}
					}
					Index++;
				}
			}
		}
	}
}

bool UGridEdgesComponent::CheckIfValidEdgeCandidate(int32 EdgeIndex, int32 TileIndex, bool bTraceForWalls, bool& AlreadyContains, int32& EdgeCost)
{
	const TMap<int32, FVector>& Locations = Grid->GetGridLocationsComponent()->GetLocations();
	const FProcedural& Procedural = Grid->GetProcedural();

	EdgeCost = 0;
	AlreadyContains = false;

	if (!Locations.Contains(EdgeIndex))
	{
		return false;
	}

	if (Edges[EdgeIndex].Edges.Contains(TileIndex))
	{
		AlreadyContains = true;
		return false;
	}

	EdgeCost = GetEdgeCostFromZDifference(Locations[EdgeIndex].Z, TileIndex);

	if (EdgeCost == 0)
	{
		return false;
	}

	// TODO test
	if (bTraceForWalls)
	{
		return !UGridBlueprintFunctionLibrary::TraceOnGrid(Grid, TileIndex, EdgeIndex, COLLISION_WALL, Procedural.TraceForWallsHeight);
	}
	else
	{
		return true;
	}
}

int32 UGridEdgesComponent::GetEdgeCostFromZDifference(float ParentZ, int32 GridIndex)
{
	const TMap<int32, FVector>& Locations = Grid->GetGridLocationsComponent()->GetLocations();
	const FProcedural& Procedural = Grid->GetProcedural();

	// Compares height difference of two tiles. Returns the appropriate edge cost between them based on the values specified.
	// If the difference is larger than the Height Impassable Cutoff, returns 0 (indicating that the edge should be removed).
	// If the difference is lower than Height Impassable Cutoff, but higher than Height Slow Increment, returns a value
	// corresponding to the height difference divided by the height slow increment.
	float HeightDiff = FMath::Abs(ParentZ - Locations[GridIndex].Z);

	if (HeightDiff < Procedural.HeightSlowIncrement)
	{
		return 1;
	}

	if (HeightDiff < Procedural.HeightImpassableCutoff)
	{
		int32 Result = HeightDiff / Procedural.HeightSlowIncrement;
		return Result + 1;
	}

	return 0;
}

void UGridEdgesComponent::AddAllTileActorEdges()
{
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AGA_Tile::StaticClass(), FoundActors);
	for (AActor* Actor : FoundActors)
	{
		AGA_Tile* TileActor = Cast<AGA_Tile>(Actor);

		AddTileEdgesToGridEdgesArray(TileActor);

		for (auto& Pair : TileActor->GetCustomEdges().Edges)
		{
			AddTileEdge(TileActor->GetGridIndex(), Pair.Key, Pair.Value);
		}
	}
}

void UGridEdgesComponent::AddTileEdgesToGridEdgesArray(AGA_Tile* Tile)
{
	check(Tile);

	FEdges LocalEdges;

	for (auto& Pair : Edges[Tile->GetGridIndex()].Edges)
	{
		// If an edge does not exist on the tile beforehand or on the added tile, remove it from the edge array
		int32 Index = Tile->GetEdges().Find(Pair.Key);
		if (Index != -1)
		{
			int32 EdgeCost = Edges[Tile->GetGridIndex()].Edges[Pair.Key];
			int32 HigherEdgeCost = FMath::Max(EdgeCost, Tile->GetEdgeCosts().Edges[Pair.Key]);
			LocalEdges.Edges.Add(Pair.Key, HigherEdgeCost);
			SetEdgeCost(Pair.Key, Tile->GetGridIndex(), HigherEdgeCost);
		}
		else
		{
			RemoveTileEdge(Pair.Key, Tile->GetGridIndex());
		}
	}

	Edges[Tile->GetGridIndex()] = LocalEdges;

	for (FTwoInts Indexes : Tile->GetBlockedCornerIndexes())
	{
		RemoveTileEdgeBothWays(Indexes.Int1, Indexes.Int2);
	}
}

void UGridEdgesComponent::RemoveUnreachableTiles()
{
	const FProcedural& Procedural = Grid->GetProcedural();

	// If the ReachableTileMarkers array is not empty, runs pathfinding from each of these markers, 
	// making any tiles that are not found impassable. Used mostly for looks, so that the hover marker does not appear over tiles the player could never reach.
	if (Procedural.ReachableTileMarks.Num() == 0)
	{
		return;
	}

	TArray<bool> LocalGridTilesToKeep;
	LocalGridTilesToKeep.SetNum(GridSizeSquared * Grid->GetZSize());

	for (FVector Location : Procedural.ReachableTileMarks)
	{
		bool bSuccess;
		int32 Index = UGridBlueprintFunctionLibrary::ConvertLocationToIndex3D(Grid, Location, bSuccess);

		FPathfindingResult Pathfinding;
		Grid->GetPathfinderComponent()->Run(Index, GridSizeSquared, EPathfindingType::PT_IgnoreCost, Pathfinding);

		for (auto& Pair : Pathfinding.PathsMap.Paths)
		{
			LocalGridTilesToKeep[Pair.Key] = true;
		}
	}

	for (int32 i = 0; i < LocalGridTilesToKeep.Num(); i++)
	{
		bool Keep = LocalGridTilesToKeep[i];
		if (!Keep)
		{
			Edges[i] = FEdges();
			Grid->GetGridLocationsComponent()->Remove(i);
		}
	}
}
