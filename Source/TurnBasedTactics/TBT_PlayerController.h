// Temp

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TBT_PlayerController.generated.h"

/**
 * Controls player mouse input and the actions associated with clicking and hovering over tiles
 */
UCLASS()
class TURNBASEDTACTICS_API ATBT_PlayerController : public APlayerController
{
	GENERATED_UCLASS_BODY()
	
public:
	/** Called from game mode. */
	void Setup();
};
