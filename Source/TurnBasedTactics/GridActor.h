// Temp

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GridActor.generated.h"

/**
 * Class for blueprints that need to find the index they are placed on and be able to snap to the grid.
 */
UCLASS()
class TURNBASEDTACTICS_API AGridActor : public AActor
{
	GENERATED_UCLASS_BODY()

	int32 GetGridIndex() const;

protected:
	/** The index on the grid array of this actor. */
	UPROPERTY(BlueprintReadOnly, Category = "Default")
	int32 GridIndex;
};
