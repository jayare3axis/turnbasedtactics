// Temp

#pragma once

#include "UnitsTypes.generated.h"

UENUM(BlueprintType)
enum class EPathfindingType : uint8
{
	PT_Standard 				UMETA(DisplayName = "Standard"),
	PT_PassThroughFriendly 		UMETA(DisplayName = "Pass Through Friendly"),
	PT_IgnoreCost				UMETA(DisplayName = "Ignore Cost"),
	PT_Big						UMETA(DisplayName = "Big"),
	PT_NoDiagonalMoovement		UMETA(DisplayName = "No Diagonal Movement"),
	PT_SimpleMixed				UMETA(DisplayName = "Simple Mixed"),
	PT_SimplePure				UMETA(DisplayName = "Simple Pure"),
	PT_SimpleIgnoreCost			UMETA(DisplayName = "Simple Ignore Cost"),
};

UENUM(BlueprintType)
enum class EFaction : uint8
{
	F_Empty		 				UMETA(DisplayName = "Standard"),
	F_Player			 		UMETA(DisplayName = "Player"),
	F_Enemy						UMETA(DisplayName = "Enemy"),
	F_Faction3					UMETA(DisplayName = "Faction 3")
};

USTRUCT(BlueprintType)
struct FAttributes
{
	GENERATED_BODY()

	/** DO NOT CHOOSE EMPTY. Which faction is the unit. Checked by other unit's AlliedFactions to determine how they interact with this unit */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	EFaction Faction;

	FAttributes()
	{
		Faction = EFaction::F_Empty;
	}
};

USTRUCT(BlueprintType)
struct FMovementOptions
{
	GENERATED_BODY()

	/** Type of pathfinding this unit is set to use by default. Properties of this pathfinding is defined in the GridManager's RunPathfinding function. */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	EPathfindingType PathfindingType;

	/** Should visibility be calculated in a diamong shape (diagonal distance counting as twice as long as straight) */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	bool bDiamondShapedVisibility;

	FMovementOptions()
	{
		PathfindingType = EPathfindingType::PT_Standard;
		bDiamondShapedVisibility = false;
	}
};
