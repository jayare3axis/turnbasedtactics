// Temp

using UnrealBuildTool;
using System.Collections.Generic;

public class TurnBasedTacticsEditorTarget : TargetRules
{
	public TurnBasedTacticsEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "TurnBasedTactics" } );
	}
}
